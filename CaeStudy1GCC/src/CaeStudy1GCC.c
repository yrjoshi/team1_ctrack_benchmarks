/**
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <string.h>

typedef struct program_trace
{
	long pid;
	int  fd;
	int  operation;
	int  sequence;

}program_trace;

typedef enum
{
	READ=1,
	WRITE,
	OPEN,
	CLOSE,
	ACCEPT
} OPCODE;


int is_equal (const program_trace T1, const program_trace T2 )
{
	if((T1.pid != T2.pid) || (T1.fd != T2.fd))
		return 0;
	return 1;
}


int less_than(program_trace T1, program_trace T2 )
{
	if(T1.pid < T2.pid)
		return 1;
	else
		if(T1.fd < T2.fd && T1.pid == T2.pid)
			return 1;
		else
			if(T1.fd == T2.fd && T1.pid == T2.pid && T1.sequence < T2.sequence)
				return 1;

	return 0;
}

char* parse_comamand_line(int argc, char *argv[], char *option, int exist)
{
	long i = 0;
	for(i = 1; i <argc; i++)
		if(strcmp(argv[i],option) == 0)
			if(!exist)
				return argv[i+1];
			else
				return argv[i];
	return 0;
}


long MAX = 32768;
int TRACE_SIZE = 2, THRESHHOLD;

long event_count = 0;
long *trace_count;
int replicates;
int ON_CPU;
struct rusage selfstats;
struct rusage selfstats1, selfstats2;
void* run_mon(void *n)
{

}
enum platform
{
	CPU=1,
	GPU,
	OPENMP
};
static int main_loop(char *pidstatus)
{
	char *line;
	char *vmpeak;

	unsigned len;

	FILE *f;
	vmpeak = NULL;
	line = (char*)malloc(128);
	len = 128;

	f = fopen(pidstatus, "r");
	if (!f) return 1;

	/* Read memory size data from /proc/pid/status */
	while (!vmpeak)
	{
		if (getline(&line, &len, f) == -1)
		{
			/* Some of the information isn't there, die */
			return 1;
		}

		/* Find VmPeak */
		if (!strncmp(line, "VmPeak:", 7))
		{
			vmpeak = strdup(&line[7]);
		}
	}
	free(line);

	fclose(f);

	/* Get rid of " kB\n"*/
	len = strlen(vmpeak);
	vmpeak[len - 4] = 0;

	/* Output results to stderr */
	fprintf(stderr, "%s\n", vmpeak);

	free(vmpeak);
	/* Success */
	return 0;
}
short find_first_of(char *str, char x, int offset)
{
	int i = offset;
	for(i = 0; i < strlen(str);i++)
		if(str[i] == x)
			break;
	return i;
}
short find_last_of(char *str, char x, int offset)
{
	int i = offset;
	for(i = offset; i >= 0;i--)
		if(str[i] == x)
			break;
	return i;
}
void substr(char *str, int offset, int len, char *dest)
{
	int i = offset, j = 0;
	for(;i < offset + len; i++)
		dest[j++]= str[i];
	dest[j] = '\0';
}
int main(int argc, char *argv[])
{

	char *filename;
	char line[1024];
	pthread_t other;
	int program_only;
	size_t len;
	char pid_str[256], fd[256];
	char processname[256];


	sprintf(processname,"/proc/%d/status",getpid());
	main_loop(processname);

	if(parse_comamand_line(argc, argv, "-c",1))
		ON_CPU = CPU;
	if(parse_comamand_line(argc, argv,"-o",1))
		ON_CPU = OPENMP;
	if(parse_comamand_line(argc, argv,"-f",0))
		filename = parse_comamand_line(argc, argv,"-f",0);
	else
	{
		fprintf(stderr,"No trace file Option -f Provided\n");
		exit(-1);
	}
	if(parse_comamand_line(argc, argv,"-l",0))
	{
		MAX = atol(parse_comamand_line(argc, argv,"-l",0));
		THRESHHOLD = MAX*TRACE_SIZE*9/10;
	}
	else
	{
		fprintf(stderr,"No trace length Option -l Provided\n");
		exit(-1);
	}

	replicates = 1;
	if(parse_comamand_line(argc, argv,"-r",0))
	{
		replicates= atoi(parse_comamand_line(argc, argv,"-r",0));
	}
	else
	{
		fprintf(stderr, "No mode Option -m Provided\n");
		exit(-1);
	}
	trace_count = (long*)malloc(sizeof(long)*MAX);
	memset(trace_count,0,sizeof(long)*MAX);
	program_trace *trace;
	FILE *trace_reader;
	long event_count = 0;
	char *str;
	size_t pos1, pos2;
	long pid;
	trace = (program_trace*)malloc(sizeof(program_trace)*MAX*TRACE_SIZE);
	trace_reader = fopen(filename,"r");
	int num_chars; char ch;
	while(!feof(trace_reader))
	{
		int operation  = 0;
		num_chars = 0;
		strcpy(line,"\0");
		for (ch = fgetc(trace_reader); ch != EOF && ch != '\n'; ch = fgetc(trace_reader)) {
			line[num_chars++] = ch;
		}
		if(ch == EOF)
			continue;
		if(strstr(line,"open(") || strstr(line,"open resumed"))
			operation  = OPEN;
		if(strstr(line,"accept(") || strstr(line,"accept resumed"))
			operation  = OPEN;
		if(strstr(line,"read("))
			operation  = READ;
		if(strstr(line,"write( "))
			operation  = WRITE;
		if(strstr(line,"close("))
			operation  = CLOSE;
		if(strstr(line,"unfinished") && operation != CLOSE)
			operation  = 0;

		if(strstr(line," ") )
		{
			short last_index = find_first_of(line,']',0);
			short first_index = find_first_of(line,' ',0);
			substr(line, first_index+1,last_index - first_index-1, pid_str);
			pid = atol((const char*)pid_str);
		}
		switch(operation)
		{
		case READ:
			pos1 = find_first_of(line, '(',0);
			pos2 = find_first_of(line, ',',pos1+1);
			substr(line,pos1+1,pos2-pos1-1,fd);
			break;
		case OPEN:
			pos1 = find_last_of(line,'=',strlen(line)-1);
			substr(line, pos1+2,strlen(line)-pos1-2,fd);
			break;
		case WRITE:
			pos1 = find_first_of(line,'(',0);
			pos2 = find_first_of(line,',',pos1+1);
			substr(line, pos1+1,pos2-pos1-1,fd);
			break;

		case CLOSE:
			pos1  = find_first_of(line,'(',0);
			pos2 = find_first_of(line,')',pos1+1);
			if(pos2 >= strlen(line))
				pos2 = find_first_of(line,' ',pos1+1);
			substr(line,pos1+1,pos2-pos1-1,fd);
			break;
		default:break;
		}
		if( operation == READ ||
			operation == WRITE ||
			operation == OPEN ||
			operation == CLOSE)
		{
			int file_desc = atoi(fd);
			long trace_count_index = pid + ((file_desc + pid)*(file_desc + pid+1))/2;
			trace_count_index  = trace_count_index  % MAX;
#if (DEBUG)
	fprintf(stderr,"row = %d, pid = %d, fd =  %d, operation = %d \n",event_count,pid, file_desc, operation);
#endif
			trace[event_count].pid = pid;
			trace[event_count].fd = file_desc;
			trace[event_count].operation = operation;
			trace[event_count].sequence = trace_count[trace_count_index]++;
			event_count++;

		}

		if(event_count >= THRESHHOLD )
		{
			//monitor code to be invoked here when no of events read goes beyond threshhold
			event_count= 0;
			break;
		}
		strcpy(pid_str,"\0");
		strcpy(fd,"\0");

	}
	if(event_count != 0)
	{
		//monitor code to be invoked here when no of events read goes beyond threshhold
		event_count= 0;
	}
	fclose(trace_reader);
	free(trace);
	free(trace_count);
	main_loop(processname);
	return 0;
}
