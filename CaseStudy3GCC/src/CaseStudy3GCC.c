/**
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
//static const int WORK_SIZE = 256;
/**
 * This macro checks return value of the CUDA runtime call and exits
 * the application if the call failed.
 */

/**
 * Host function that prepares data array and passes it to the CUDA kernel.
 */
typedef struct program_trace
{
	long objectid;
	int  sequence;
	long bandwidth;

} program_trace;


int is_equal (const program_trace T1, const program_trace T2 )
{
	if(T1.objectid != T2.objectid)
		return 0;
	return 1;
}


int less_than (const program_trace T1, const program_trace T2 )
{
	if(T1.objectid < T2.objectid)
		return 1;
	else
		if(T1.sequence < T2.sequence && T1.objectid == T2.objectid)
			return 1;

	return 0;
}

char* parse_comamand_line(int argc, char *argv[], char *option, int exist)
{
	long i = 0;
	for(i = 1; i <argc; i++)
		if(strcmp(argv[i],option) == 0)
		{
			if(!exist)
				return argv[i+1];
			else
				return argv[i];
		}
	return 0;
}


long MAX = 32768;
int TRACE_SIZE = 2, THRESHHOLD;

long event_count = 0;
long *trace_count;
int replicates;
int ON_CPU;
struct rusage selfstats;
struct rusage selfstats1, selfstats2;
void* run_mon(void *n)
{
}
static int main_loop(char *pidstatus)
{
	char *line;
	char *vmpeak;

	size_t len;

	FILE *f;
	vmpeak = NULL;
	line = (char*)malloc(128);
	len = 128;

	f = fopen(pidstatus, "r");
	if (!f) return 1;

	/* Read memory size data from /proc/pid/status */
	while (!vmpeak)
	{
		if (getline(&line, &len, f) == -1)
		{
			/* Some of the information isn't there, die */
			return 1;
		}

		/* Find VmPeak */
		if (!strncmp(line, "VmPeak:", 7))
		{
			vmpeak = strdup(&line[7]);
		}
	}
	free(line);

	fclose(f);

	/* Get rid of " kB\n"*/
	len = strlen(vmpeak);
	vmpeak[len - 4] = 0;

	/* Output results to stderr */
	fprintf(stderr, "%s\n", vmpeak);

	free(vmpeak);
	/* Success */
	return 0;
}
char *line_tok[1024];
char* get_field(const char *line, int count)
{
	int j = 0;
	strcpy(line_tok, line);
	char *token = strtok(line_tok," "); j++;
	while(token != NULL && j <= count)
	{
		token = strtok(NULL, " "); j++;
	}
	return token;
}
short find_first_of(char *str, char x, int offset)
{
	int i = offset;
	for(i = 0; i < strlen(str);i++)
		if(str[i] == x)
			break;
	return i;
}
short find_last_of(char *str, char x, int offset)
{
	int i = offset;
	for(i = offset; i >= 0;i--)
		if(str[i] == x)
			break;
	return i;
}
void substr(char *str, int offset, int len, char *dest)
{
	strcpy(dest,"");
	int i = offset, j = 0;
	for(;i < offset + len; i++)
		dest[j++]= str[i];
	dest[j] = '\0';
}
enum platform
{
	CPU=1,
	GPU,
	OPENMP
};

int main(int argc, char *argv[])
{

	char *filename;
	char line[1024];
	pthread_t other;

	int program_only;
	char processname[256];
	sprintf(processname,"/proc/%d/status",getpid());
	main_loop(processname);
	if(parse_comamand_line(argc,argv,"-c",1))
		ON_CPU = CPU;
	if(parse_comamand_line(argc,argv,"-o",1))
		ON_CPU = OPENMP;
	if(parse_comamand_line(argc,argv,"-f",0))
	{
		filename = parse_comamand_line(argc,argv,"-f",0);
	}
	else
	{
		fprintf(stderr,"No trace file Option -f Provided\n");
		exit(-1);
	}
	if(parse_comamand_line(argc, argv,"-l",0))
	{
		MAX = atol(parse_comamand_line(argc, argv,"-l",0));
		THRESHHOLD = MAX*TRACE_SIZE*9/10;
	}
	else
	{
		fprintf(stderr,"No trace file Option -l Provided\n");
		exit(-1);
	}

	replicates = 1;
	if(parse_comamand_line(argc, argv,"-r",0))
	{
		replicates= atoi(parse_comamand_line(argc, argv,"-r",0));
	}
	else
	{
		fprintf(stderr, "No mode Option -m Provided\n");
		exit(-1);
	}
	trace_count = (long*)malloc(sizeof(long)*MAX);
	memset(trace_count,0,sizeof(long)*MAX);
	program_trace *trace;

	FILE *trace_reader;
	long event_count = 0;
	trace_reader = fopen(filename,"r");
	trace = (program_trace*)malloc(sizeof(program_trace)*MAX*TRACE_SIZE);;
	char *ip, first_3_of_ip[15], ip_part_4_str[4],ip_part_3_str[4];
	short pos1, third_dot;
	int num_chars; char ch;
	while(!feof(trace_reader))
	{
		num_chars = 0;
		strcpy(line,"\0");
		for (ch = fgetc(trace_reader); ch != EOF && ch != '\n'; ch = fgetc(trace_reader)) {
			line[num_chars++] = ch;
		}
		if(ch == EOF)
			continue;
		// null-terminate the string
		line[num_chars] = '\0';
		ip = get_field(line, 2);
		float bandwidth = atof(get_field(line,12))/atof(get_field(line,10));
		pos1 = find_last_of(ip,'.',strlen(ip)-1)+1;
		substr(ip, pos1, strlen(ip)-pos1,ip_part_4_str);
		short ip_part_4 = (short)atoi(ip_part_4_str);
		pos1 = find_last_of(ip, '.', strlen(ip)-1)-1;
		substr(ip, 0, pos1, first_3_of_ip);
		third_dot = find_last_of(first_3_of_ip,'.',strlen(first_3_of_ip)-1);
		substr(ip,third_dot+1,pos1-third_dot,ip_part_3_str);
		short ip_part_3 =  (short)atoi(ip_part_3_str);

		long index = ip_part_3 + ((ip_part_4 + ip_part_3)*(ip_part_4 + ip_part_3+1))/2;
		long trace_count_index = index % (MAX);

#if (DEBUG)
		fprintf(stderr,"row = %d, ip_part_4 = %d,ip_part_3 =  %d, bytes sent = %s, time = %s\n",event_count,ip_part_4, ip_part_3, get_field(line,12), get_field(line,10));
#endif
		trace[event_count].objectid = index;
		trace[event_count].sequence = trace_count[trace_count_index]++;
		trace[event_count++].bandwidth = bandwidth;

		if(event_count >= THRESHHOLD )
		{
			//monitor code to be invoked here when no of events read goes beyond threshhold
			event_count= 0;
			break;
		}

	}
	if(event_count != 0 )
	{
		//monitor code to be invoked here when no of events read goes beyond threshhold
		event_count= 0;
	}
	fclose(trace_reader);
	free(trace);
	free(trace_count);
	main_loop(processname);
	return 0;
}
